import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:stopwatch_flutter/ui/reset_button.dart';
import 'package:stopwatch_flutter/ui/start_stop_button.dart';
import 'package:stopwatch_flutter/ui/stopwatch_renderer.dart';

/// We need :
/// - a DateTime object to capture the start time
/// - a Timer callback to calculate the "elapsed time"
class Stopwatch extends StatefulWidget {
  @override
  _StopwatchState createState() => _StopwatchState();
}

class _StopwatchState extends State<Stopwatch>
    with SingleTickerProviderStateMixin {
  Duration _previousElapsed = Duration.zero;
  Duration _currentElapsed = Duration.zero;

  Duration get _elapsed => _previousElapsed + _currentElapsed;

  bool _isRunning = false;

  // We replace the Timer with a Ticker where we don't have to set a Duration delay.
  // Ticker automatically syncs to the screen refresh rate
  // We use 'late' to indicate we want this variable to be non-nullable and that we initialize later when calling initState()
  late final Ticker _ticker;

  /// This method will be called before 'build()' to initialize some variables
  @override
  void initState() {
    super.initState();
    _ticker = this.createTicker((elapsed) {
      // TODO : Replace setState to avoid rebuilding every widgets (even the static ones) at 60 fps in this page for Better performance
      setState(() {
        _currentElapsed = elapsed;
      });
    });
  }

  /// When exiting the Widget, we have to release resources
  @override
  void dispose() {
    _ticker.dispose();
    super.dispose();
  }

  void _toggleRunning() {
    // TODO : Replace setState to avoid rebuilding every widgets (even the static ones) at 60 fps in this page for Better performance
    setState(() {
      _isRunning = !_isRunning;
      if (_isRunning) {
        _ticker.start();
      } else {
        _ticker.stop();
        _previousElapsed += _currentElapsed;
        _currentElapsed = Duration.zero;
      }
    });
  }

  void _reset() {
    _ticker.stop();

    // TODO : Replace setState to avoid rebuilding every widgets (even the static ones) at 60 fps in this page for Better performance
    setState(() {
      _isRunning = false;
      _previousElapsed = Duration.zero;
      _currentElapsed = Duration.zero;
    });
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        // In stopwatch.dart we are making all business logic. stopwatch_renderer.dart will only manage the UI (Single Responsability Principle)
        // To calculate the size of a widget explicitly, MediaQuery and LayoutBuilder are useful
        // constraints.maxWidth gives us the Max width of the child widget
        final radius = constraints.maxWidth / 2;
        return Stack(
          children: [
            StopwatchRenderer(
              elapsed: _elapsed,
              radius: radius,
            ),
            Align(
              alignment: Alignment.bottomLeft,
              child: SizedBox(
                width: 80,
                height: 80,
                child: ResetButton(
                  onPressed: _reset,
                ),
              ),
            ),
            Align(
              alignment: Alignment.bottomRight,
              child: SizedBox(
                width: 80,
                height: 80,
                child: StartStopButton(
                  isRunning: _isRunning,
                  onPressed: _toggleRunning,
                ),
              ),
            ),
          ],
        );
      },
    );
  }
}
