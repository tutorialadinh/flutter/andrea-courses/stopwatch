import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:stopwatch_flutter/ui/stopwatch.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      // Dark background is not black by default
      theme: ThemeData.dark().copyWith(
        // Force the background for dark theme to be black
        scaffoldBackgroundColor: Colors.black,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  MyHomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      // With 'AnnotatedRegion' we set the Text Color of our system to white to set the hour and our battery (for iOS, not Android)
      // We use AnnotatedRegion to customise the system UI overlay on a screen-by-screen basis
      value: SystemUiOverlayStyle.light,
      child: Scaffold(
        body: Center(
          child: Padding(
            padding: const EdgeInsets.all(32.0),
            child: AspectRatio(
                // We want our Stopwatch (a circle shape) to fit inside the square shape
                // Here we tweak the AspectRatio so that the child widget will be slightly taller than wide
                aspectRatio: 0.85,
                child: Stopwatch()),
          ),
        ),
      ),
    );
  }
}
